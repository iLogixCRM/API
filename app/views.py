from django.shortcuts import render
from .serializers import *
from rest_framework.response import Response
from django.http import HttpResponse
from rest_framework import status, generics
from rest_framework.views import APIView
from rest_framework.status import (
	HTTP_200_OK,
	HTTP_201_CREATED,
	HTTP_400_BAD_REQUEST,
	HTTP_401_UNAUTHORIZED,
	HTTP_404_NOT_FOUND
)
from django.views.generic import TemplateView
from rest_framework.parsers import FileUploadParser, MultiPartParser, FormParser
from django.utils.datastructures import MultiValueDictKeyError
import os
import io
import csv
import json
import uuid
from django.conf import settings
import chardet
from celery.result import AsyncResult
from datetime import datetime, timedelta, date, time
from django.apps import apps
from .lead_templates import *
from .report_csv import *

from django.template.response import TemplateResponse
from django.template.loader import get_template
from django.core.mail import EmailMessage
from dateutil import relativedelta

class HomePageView(TemplateView):
	def get(self, request, **kwargs):
		return render(request, 'index.html', context=None)

def generateUID():
	uid = uuid.uuid4().hex[:10]
	try:
		UserUpdateView.objects.get(uid=uid)
	except UserUpdateView.DoesNotExist:
		return uid
	else:
		return generateUID()

def log400Error(error):
	try:
		timestamp = datetime.now().time().strftime("%H:%M:%S")
		dateToday = datetime.now().date()
		f = open("logs/400Error_log","a+")
		f.write(str(dateToday)+' '+ str(timestamp) +': '+ str(error)+'\n')
		f.close()
	except Exception as e:
		log400Error(e)
		raise e

class UploadLead(APIView):
	def post(self, request, format=None):
		try:
			file_data = request.FILES['file']
			read_file = request.FILES['read']
			user_uploading = request.data['user']

			existing_data = 0
			saved_data = 0
			user = User.objects.get(id=user_uploading)
			if file_data.name.endswith('.csv'): 
				try:
					file_type = 'csv'
					encoding = chardet.detect(read_file.read())
					file_data = file_data.read().decode(encoding['encoding'])		
					io_string = io.StringIO(file_data)
					csv_reader = csv.reader(io_string, delimiter=',')
					csv_reader = list(csv_reader)
					line_count = 0
					i = 0
					pr = 0
					for row in csv_reader:
						if line_count == 0:
							column_row = row
							line_count += 1
						else:
							row_data_count = 0
							try:
								column_row = [x.strip(' ') for x in column_row]
								zipbObj = zip(column_row, row)
								dictOfWords = dict(zipbObj)

								if (dictOfWords['cell'] == ''):
									return Response({"ErrorBlank": 'Blank cell at row: '+ str(csv_reader.index(row)+1)}, status=status.HTTP_400_BAD_REQUEST)
								else:
									existing_leads = Lead.objects.filter(cell=dictOfWords['cell']).values()
									if not existing_leads:
										saved_data += 1
										leads = Lead.objects.create(
															cell=dictOfWords['cell'],
															name=dictOfWords['name'],
															location=dictOfWords['location'],
														)
									else:
										existing_data += 1
							except Exception as e:
								log400Error(e)
								return Response({"ErrorHere": str(e)}, status=status.HTTP_400_BAD_REQUEST)
				except Exception as e:
					log400Error(e)
					return Response({"Error2": str(e)}, status=status.HTTP_400_BAD_REQUEST)
			else:
				file_type = 'Not suported'
				return Response({'Error': 'File format not suported'}, status=status.HTTP_400_BAD_REQUEST)
		except Exception as e:
			log400Error(e)
			return Response({'Error2': str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data={"saved": saved_data, "duplicates": existing_data}, status=status.HTTP_201_CREATED)

class LeadTemplateView(APIView):
	def get(self, request, format=None):
		try:
			return leadsTemplate()
		except Exception as e:
			log400Error(e)
			return Response({"Error": str(e)}, status = status.HTTP_400_BAD_REQUEST)
		return Response(status = status.HTTP_200_OK)

class GetLeadsAPIView(APIView):
	def get(self, request, format=None):
		try:
			# leads = Lead.objects.filter(dialed=False, on_call=False, attempts__lte=6).values('cell', 'id').order_by('id')[:10]
			leads = Lead.objects.filter(dialed=False, on_call=False).values('cell', 'id').order_by('id')[:10]
		except Exception as e:
			log400Error(e)
			return Response({'Error2': str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=leads, status=status.HTTP_201_CREATED)	

def callStatus(lead):
	try:
		lead.on_call = True
		lead.save()
		lead_status = False
		return lead_status
	except Exception as e:
		log400Error(e)
		raise e

class OnCallStatusAPIView(APIView):
	def post(self, request, format=None):
		try:
			pk = request.data['id']
			lead = Lead.objects.get(id=pk)
			lead_status = True if lead.on_call == True else callStatus(lead)
		except Exception as e:
			log400Error(e)
			return Response({'Error': str(e)}, status=status.HTTP_400_BAD_REQUEST)	
		return Response(data=lead_status, status=status.HTTP_201_CREATED)

class LeadDetailsAPIView(APIView):
	def get(self, request, pk, format=None):
		try:
			lead_details = Lead.objects.filter(id=pk).values()[0]
			kwargs = {'lead_id': pk}
			try:
				prevoius_call = CustomerCall.objects.filter(**kwargs).values().order_by('-id')[0]
			except Exception as e:
				log400Error(e)
				if str(e) == 'list index out of range':
					prevoius_call = False
			info = {
				'lead_details': lead_details,
				'prevoius_call': prevoius_call,
			}
		except Exception as e:
			log400Error(e)
			return Response({'Error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=info, status=status.HTTP_200_OK)

class CreateCallAPIView(APIView):
	def post(self, request, format=None):
		try:
			call_data = request.data
			lead_id = request.data['lead']
			agent_id = request.data['agent']
			agent_instance = User.objects.get(id=agent_id)

			lead = Lead.objects.get(id=lead_id)
			lead.on_call = False
			lead.dialed = True
			lead.attempts = lead.attempts + 1

			if (call_data['lead_status'] == 'Reachable'):
				lead.contacted = True
				if 'dispose' in call_data:
					if call_data['dispose'] == 'Call Back':
						try:
							callbck_q = CallBackQue.objects.get(lead_id=lead_id,cell=lead.cell)
							callbck_q.datetocall = call_data['datetocall']
							callbck_q.timetocall = call_data['timetocall']
							callbck_q.save()
						except (CallBackQue.DoesNotExist, CallBackQue.MultipleObjectsReturned) as e:
							if str(e) == 'CallBackQue matching query does not exist.':	
								CallBackQue.objects.create(lead_id=lead_id,agent_id=agent_id,cell=lead.cell,datetocall=call_data['datetocall'],timetocall=call_data['timetocall'],)

					elif call_data['dispose'] == 'Complete Call':
						lead.call_complete = True
						try:
							callback_que = CallBackQue.objects.get(lead_id=lead_id,cell=lead.cell,)
							callback_que.completed = True
							callback_que.save()
						except (CallBackQue.DoesNotExist, CallBackQue.MultipleObjectsReturned) as e:
							if str(e) == 'CallBackQue matching query does not exist.':	
								pass
					else:
						try:
							callback_que = CallBackQue.objects.get(lead_id=lead_id,cell=lead.cell,)
							callback_que.completed = True
							callback_que.save()
						except (CallBackQue.DoesNotExist, CallBackQue.MultipleObjectsReturned) as e:
							if str(e) == 'CallBackQue matching query does not exist.':	
								pass
			elif (call_data['lead_status'] == 'Un Reachable'):
				try:
					callback_que = CallBackQue.objects.get(lead_id=lead_id,cell=lead.cell,)
					callback_que.completed = True
					callback_que.save()
				except (CallBackQue.DoesNotExist, CallBackQue.MultipleObjectsReturned) as e:
					if str(e) == 'CallBackQue matching query does not exist.':	
						pass
				try:
					unreachable_que = UnReachableQue.objects.get(lead_id=lead.id,cell=lead.cell)
					attempts = int(unreachable_que.count)
					if attempts < 6:
						unreachable_que.count = attempts+1
						unreachable_que.save()
					else:
						unreachable_que.max_reached = True
						unreachable_que.save()
				except (UnReachableQue.DoesNotExist, UnReachableQue.MultipleObjectsReturned) as e:
					if str(e) == 'UnReachableQue matching query does not exist.':	
						UnReachableQue.objects.create(count= 1,lead_id=lead.id,cell=lead.cell)
				else:
					pass
			lead.save()
			call_data['lead'] = lead
			call_data['agent'] = agent_instance
			new_call_data = CustomerCall.objects.create( **call_data)
		except Exception as e:
			log400Error(e)
			return Response({'Error': str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(status=status.HTTP_201_CREATED)

class CallBacksAPIView(APIView):
	def post(self, request, format = None):
		try:
			dateOne = '2019-08-01'
			dateTwo = '2019-08-13'
			agent_id = request.data['user_id']
			dateToday = str(datetime.now().date())
			get_data = CallBackQue.objects.filter(
												# agent_id=agent_id,
												completed=False,
												datetocall__range=[dateOne, dateTwo]
												# datetocall__contains=dateToday
											).values('cell' , 'lead_id', 'timetocall', 'datetocall', 'completed')
			incomp_calls = list({(v['cell']):v for v in get_data}.values())
		except Exception as e:
			log400Error(e)
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=incomp_calls, status=status.HTTP_200_OK)			

class UnreachablesAPIView(APIView):
	def get(self, request, format = None):
		try:
			unreachables = UnReachableQue.objects.filter(max_reached=False, count__lte=6).values().order_by('-id')[:10]
			results = []
			for x in unreachables:
				lead_contact = Lead.objects.get(id=x['lead_id'])
				if lead_contact.contacted == False:
					results.append(x)
		except Exception as e:
			log400Error(e)
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=results, status=status.HTTP_200_OK)			

def filterReport(datefrom, dateto, format=None):
	try:
		report_data = []
		get_data = CustomerCall.objects.filter(created_at__range=[datefrom, dateto]).values().order_by('id')
		calls = list({(v['lead_id']):v for v in get_data}.values())

		if calls:
			for data in calls:
				if data['call_start'] == None and data['call_end'] == None:
					data['call_duration'] = ''
				else:
					data['call_duration'] = str(getHt(data['call_start'], data['call_end']))+ ' sec'
				user = User.objects.get(id=data['agent_id'])
				data['agent'] = str(user.first_name)+' '+str(user.last_name)
				lead_data = Lead.objects.filter(id=data['lead_id']).values()[0]
				report_data.append({'lead_data': lead_data, 'call_data': data})	
	except Exception as e:
		log400Error(e)
		raise e
	return report_data

class ReportAPIView(APIView):
	def get(self, request, datefrom, dateto, format=None):
		try:
			datefrom = datefrom
			dateto = datetime.strptime(dateto, '%Y-%m-%d').date() + timedelta(+1)
			report_data = filterReport(datefrom, dateto)
		except Exception as e:
			log400Error(e)
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=report_data, status=status.HTTP_200_OK)

class DownloadReportAPIView(APIView):
	def get(self, request, datefrom, dateto, format=None):
		try:
			datefrom = datefrom
			dateto = datetime.strptime(dateto, '%Y-%m-%d').date() + timedelta(+1)
			report_data = filterReport(datefrom, dateto)
			return downloadReport(report_data)
		except Exception as e:
			log400Error(e)
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=report_data, status=status.HTTP_200_OK)

def getHt(call_start, call_end):
	try:
		call_start = datetime.strptime(call_start, "%Y-%m-%d %H:%M:%S")
		call_end = datetime.strptime(call_end, "%Y-%m-%d %H:%M:%S")
		difference = relativedelta.relativedelta(call_end, call_start)
		sec_diff = difference.hours*3600 + difference.minutes*60 + difference.seconds
		return sec_diff
	except Exception as e:
		raise e

class onCallLeadsAPIView(APIView):
	def get(self, request, format=None):
		try:
			Lead.objects.filter(on_call=True).update(on_call=False)
		except Exception as e:
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(status=status.HTTP_200_OK)

class updateLeadsAPIView(APIView):
	def get(self, request, format=None):
		try:
			leads = Lead.objects.filter(dialed=True).count()
		except Exception as e:
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(data=leads, status=status.HTTP_200_OK)

	def post(self, request, format=None):
		try:
			leads = ['254792785881','254719153710','254796717653','254726665613','254722112128','254757184399','254718951179','254729306926','254718319770','254796931038','254726842208','254799305812','254710314737','254729620638','254713961642','254717785205','254721707885','254728756766','254729483939','254720229983','254729371263','254724082891','254704081326','254716008110','254722433682','254713797064','254796023526','254714395766','254717824383','254799293672','254728744282','254710368862','254726313512','254728628689','254704693800','254728896395','254727687953','254706885000','254718064743','254728082696','254717847346','254712247816','254724388134','254724202785','254722831622','254705685102','254722432515','254796768384','254722694003','254722928298','254726208719','254700461717','254701952384','254705533145','254729467227','254707654187','254714072510','254715737205','254714063299','254708691142','254715931990','254708536122','254711298759','254717414359','254711894745','254726621227','254743180713','254718472441','254710474444','254726430808','254741160094','254729131216','254726717009','254740440780','254721945670','254711393152','254720243128','254712141015','254722531610','254722298965','254720223441','254712835796','254723674138','254718644673','254711821524','254722453313','254743575829','254727574856','254726022067','254795430081','254729259157','254714816833','254725838510','254720785085','254711321947','254721935503','254716507902','254702632056','254712190598','254713154696','254713119699','254745105499','254799270066','254705345982','254727733059','254795276791','254705888056','254700095628','254727455070','254723395999','254725454191','254740907947','254727346872','254706406217','254705963210','254726325018','254721468822','254715140753','254723578799','254723461778','254727387616','254715972164','254745848085','254714645926','254708988880','254714029400','254700001204','254726939839','254729939704','254720739444','254704049918','254720216938','254710199938','254799498958','254722993098','254722923590','254790357982','254799464339','254790900897','254715296428','254717863117','254721319827','254724331299','254720864939','254729598985','254701992229','254795338482','254720531516','254705468974','254723461652','254721513125','254727044682','254718335211','254743338515','254721447290','254720729359','254729923239','254717228255','254729973974','254710329493','254702000722','254710965713','254705340810','254707414193','254724014953','254711822701','254714661921','254707211165','254717826806','254700744608','254727365922','254701770352','254716497110','254723634587','254723999228','254719695878','254791252370','254725422957','254719787458','254716499225','254705477775','254711335654','254712021889','254723310134','254723380685','254705288762','254720102338','254720851587','254720261525','254705087554','254742034614','254720315648','254721116394','254710610930','254722455884','254724020198','254758916579','254727644397','254715267815','254721748382','254716533665','254712843587','254707548323','254704356548','254729359937','254722284434','254717531804','254715551260','254721151264','254725878412','254723928555','254726880352','254727275174','254729945352','254707188151','254726804413','254726520169','254742664294','254703966769','254700280753','254702974365','254727142086','254799844921','254702601007','254720091225','254727226773','254716132772','254724684782','254721125797','254710765241','254706410898','254727686876','254722214839','254729037211','254713724556','254706497024','254724956530','254748516769','254701236150','254721776122','254720348515','254701738493','254726562468','254791658191','254729152241','254796582882','254743991135','254723537942','254723577650','254712505468','254719130817','254700382114','254797710546','254798426179','254725466428','254719235382','254723413395','254704620600','254797117870','254720410615','254721223706','254716530204','254715952050','254721911225','254728417762','254720616632','254720936270','254722382812','254720514671','254727162531','254721897173','254797454949','254704450587','254711657295','254795443906','254726585259','254725094237','254719689466','254796253573','254714721501','254710468718','254795595230','254726745107','254728879442','254759001320','254704243725','254743897958','254724418542','254746934125','254718650633','254746480503','254748519103','254701271303','254715578215','254701399040','254722854514','254729038696','254723646506','254710830235','254726527215','254720376514','254714695459','254717281740','254700103447','254723410005','254702299406','254723416040','254795662480','254705710082','254722845350','254725886867','254721295638','254724851918','254722172808','254724032917','254713685443','254723584187','254723947342','254727310121','254748811296','254793915653','254716122946','254710725958','254725951397','254722606109','254720724509','254768670727','254725264814','254721993478','254713686356','254710328201','254720996562','254712311472','254757656723','254796950137','254720748739','254705749420','254700573733','254729776939','254725348090','254714675387','254712911249','254701848218','254716787465','254723602257','254726861004','254720413134','254791580951','254720660315','254722321010','254797329019','254725447737','254720143211','254727807301','254719439593','254724839883','254710859229','254711812112','254799665300','254792922048','254721307510','254717486819','254728630003','254720631536','254721504313','254726998413','254726796821','254795108016','254720304198','254725841742','254724418679','254715089834','254719384070','254725108712','254795414664','254723977821','254728524239','254702829195','254710690977','254723339917','254720838226','254743419077','254721581249','254745924590','254723348121','254741281363','254723309262','254721301679','254727313681','254743581088','254723044721','254723120387','254745937190','254703723947','254728338395','254705725752','254722324286','254728321843','254719744063','254707434688','254740641859','254723577040','254720600541','254746170739','254727540070','254714904069','254758018184','254721551444','254713055805','254795649300','254726484263','254724516264','254712177013','254729563596','254703458108','254720605956','254729620969','254712747911','254717385881','254715420243','254719245336','254791151583','254704425087','254716093805','254701192868','254728823358','254726930896','254711886002','254716930587','254721314202','254746970101','254711210132','254743897964','254715950602','254717204158','254713056556','254741413948','254768898099','254727566598','254725584342','254723994008','254728969914','254720984209','254701394974','254713329636','254713840477','254757276747','254712640360','254729829367','254727942084','254729840418','254724894142','254720601160','254726748906','254724772332','254721307591','254724454772','254717150706','254715592484','254721301702','254724941119','254703787575','254713363624','254725034585','254792661469','254722893827','254721240781','254726478304','254725205808','254705021831','254713976906','254715433202','254728273634','254724224365','254701301063','254715744482','254702046225','254715733658','254721299894','254759872424','254727686971','254742480505','254724693230','254705159490','254759378729','254727435228','254791580431','254723335856','254724345599','254729957791','254729118666','254728162455','254703450528','254719578094','254723014362','254718602531','254720597901','254719796191','254712239635','254721917663','254702763875','254725670461','254708103299','254795586014','254726124797','254717214768','254715432442','254725601677','254722985174','254723557859','254707232727','254718725503','254757979123','254727383997','254715534925','254719193900','254757045817','254792490856','254704264089','254796329727','254703670556','254708043719','254716832467','254710535420','254740550602','254799092910','254713051854','254725064715','254726046647','254721245550','254727809595','254746891596','254718749947','254725767080','254728466011','254796500383','254723245995','254724431579','254703714975','254701717544','254708190874','254729167529','254724280322','254710686939','254713485048','254720486017','254720694169','254721911532','254720760972','254723918566','254722950216','254729019438','254720753187','254722266418','254713839530','254721777237','254703653966','254713748007','254724393313','254795820462','254728053918','254725546625','254724529839','254726357861','254727215762','254790254292','254724667659','254722759054','254721679368','254715366023','254759750446','254720027554','254716173202','254707020793','254727851202','254715234948','254724566962','254719121846','254720338591','254719575303','254705338544','254746800333','254727780126','254797867595','254797195432','254703690366','254745092231','254790839689','254701437952','254727252701','254712142196','254711807207','254724577354','254703814861','254724889985','254700154146','254704828984','254724965821','254720109477','254718415560','254723576697','254714123460','254728155604','254710144142','254720692714','254702803080','254796547583','254710131666','254718012748','254720040156','254768581294','254705192996','254700467146','254729622678','254759219472','254723617852','254727922028','254722577018','254726601792','254720262080','254758668589','254702437392','254791826044','254727748724','254799408104','254715787219','254729916514','254712662990','254717263188','254727133546','254714331059','254722833343','254702121619','254705496723','254746473432','254728520993','254720833862']
			for i in leads:
				lead = Lead.objects.get(cell=i)
				lead.dialed = False
				lead.save()
		except Exception as e:
			return Response({"Error": str(e)}, status=status.HTTP_400_BAD_REQUEST)
		return Response(status=status.HTTP_200_OK)
