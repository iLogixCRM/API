from django.db import models
from accounts.models import *

class Lead(models.Model):
	cell = models.CharField(max_length=1000, null=True, blank=True)
	name = models.CharField(max_length=1000, null=True, blank=True)
	location = models.CharField(max_length=1000, null=True, blank=True)

	attempts = models.IntegerField(default=0)
	contacted = models.BooleanField(default=False)
	call_complete = models.BooleanField(default=False)
	on_call = models.BooleanField(default=False)
	dialed = models.BooleanField(default=False)
	updated_at = models.DateTimeField(null=False, blank=False, auto_now=True)
	created_at = models.DateTimeField(null=False, blank=False, auto_now_add=True)

	class Meta:
		db_table = "Lead"
		verbose_name = "Lead"
		verbose_name_plural = "Leads"

	def __str__(self):
		return u'%s %s' % (self.id, self.cell)

class CustomerCall(models.Model):
	q1 = models.CharField(max_length=1000, null=True, blank=True)
	q2 = models.CharField(max_length=1000, null=True, blank=True)
	q3 = models.CharField(max_length=1000, null=True, blank=True)
	q4 = models.CharField(max_length=1000, null=True, blank=True)
	q5 = models.CharField(max_length=1000, null=True, blank=True)
	q6 = models.CharField(max_length=1000, null=True, blank=True)
	q7 = models.CharField(max_length=1000, null=True, blank=True)
	q8 = models.CharField(max_length=1000, null=True, blank=True)
	q9 = models.CharField(max_length=1000, null=True, blank=True)
	q10 = models.CharField(max_length=1000, null=True, blank=True)
	q11 = models.CharField(max_length=1000, null=True, blank=True)
	q12 = models.CharField(max_length=1000, null=True, blank=True)
	q13 = models.CharField(max_length=1000, null=True, blank=True)
	q13_other = models.CharField(max_length=1000, null=True, blank=True)
	q14 = models.CharField(max_length=1000, null=True, blank=True)
	q15 = models.CharField(max_length=1000, null=True, blank=True)
	q15_other = models.CharField(max_length=1000, null=True, blank=True)
	q16 = models.CharField(max_length=1000, null=True, blank=True)
	q17 = models.CharField(max_length=1000, null=True, blank=True)
	q17_other = models.CharField(max_length=1000, null=True, blank=True)
	q18 = models.CharField(max_length=1000, null=True, blank=True)
	q19 = models.CharField(max_length=1000, null=True, blank=True)
	q20 = models.CharField(max_length=1000, null=True, blank=True)
	q21 = models.CharField(max_length=1000, null=True, blank=True)
	q22 = models.CharField(max_length=1000, null=True, blank=True)
	q23 = models.CharField(max_length=1000, null=True, blank=True)
	q24 = models.CharField(max_length=1000, null=True, blank=True)
	q24_other = models.CharField(max_length=1000, null=True, blank=True)
	q25 = models.CharField(max_length=1000, null=True, blank=True)
	q26 = models.CharField(max_length=1000, null=True, blank=True)
	q27 = models.CharField(max_length=1000, null=True, blank=True)
	q28 = models.CharField(max_length=1000, null=True, blank=True)
	q29 = models.CharField(max_length=1000, null=True, blank=True)
	q30 = models.CharField(max_length=1000, null=True, blank=True)
	q31 = models.CharField(max_length=1000, null=True, blank=True)
	q31_other = models.CharField(max_length=1000, null=True, blank=True)
	q32 = models.CharField(max_length=1000, null=True, blank=True)
	q33 = models.CharField(max_length=1000, null=True, blank=True)
	q34 = models.CharField(max_length=1000, null=True, blank=True)
	q35 = models.CharField(max_length=1000, null=True, blank=True)
	q36 = models.CharField(max_length=1000, null=True, blank=True)
	q36_other = models.CharField(max_length=1000, null=True, blank=True)
	q37 = models.CharField(max_length=1000, null=True, blank=True)
	q37 = models.CharField(max_length=1000, null=True, blank=True)
	q38 = models.CharField(max_length=1000, null=True, blank=True)
	q39 = models.CharField(max_length=1000, null=True, blank=True)
	q40 = models.CharField(max_length=1000, null=True, blank=True)
	q41 = models.CharField(max_length=1000, null=True, blank=True)
	q42 = models.CharField(max_length=1000, null=True, blank=True)
	q43 = models.CharField(max_length=1000, null=True, blank=True)
	q44 = models.CharField(max_length=1000, null=True, blank=True)
	q45 = models.CharField(max_length=1000, null=True, blank=True)
	q46 = models.CharField(max_length=1000, null=True, blank=True)
	q47 = models.CharField(max_length=1000, null=True, blank=True)
	q47_other = models.CharField(max_length=1000, null=True, blank=True)
	q49 = models.CharField(max_length=1000, null=True, blank=True)
	q48 = models.CharField(max_length=1000, null=True, blank=True)
	q50 = models.CharField(max_length=1000, null=True, blank=True)
	q51 = models.CharField(max_length=1000, null=True, blank=True)
	q52 = models.CharField(max_length=1000, null=True, blank=True)
	q53 = models.CharField(max_length=1000, null=True, blank=True)
	q54 = models.CharField(max_length=1000, null=True, blank=True)
	q55 = models.CharField(max_length=1000, null=True, blank=True)
	q56 = models.CharField(max_length=1000, null=True, blank=True)
	q57 = models.CharField(max_length=1000, null=True, blank=True)
	q58 = models.CharField(max_length=1000, null=True, blank=True)
	q58_other = models.CharField(max_length=1000, null=True, blank=True)
	q59 = models.CharField(max_length=1000, null=True, blank=True)
	q60 = models.CharField(max_length=1000, null=True, blank=True)
	q60_acres = models.CharField(max_length=1000, null=True, blank=True)
	q61 = models.CharField(max_length=1000, null=True, blank=True)
	q62 = models.CharField(max_length=1000, null=True, blank=True)
	q63 = models.CharField(max_length=1000, null=True, blank=True)
	q64 = models.CharField(max_length=1000, null=True, blank=True)
	q65 = models.CharField(max_length=1000, null=True, blank=True)
	q66 = models.CharField(max_length=1000, null=True, blank=True)
	q67 = models.CharField(max_length=1000, null=True, blank=True)
	q67_other = models.CharField(max_length=1000, null=True, blank=True)
	q68 = models.CharField(max_length=1000, null=True, blank=True)
	q69 = models.CharField(max_length=1000, null=True, blank=True)
	q70 = models.CharField(max_length=1000, null=True, blank=True)
	q71 = models.CharField(max_length=1000, null=True, blank=True)
	q72 = models.CharField(max_length=1000, null=True, blank=True)
	gender = models.CharField(max_length=1000, null=True, blank=True)
	consent = models.CharField(max_length=1000, null=True, blank=True)

	datetocall 	= models.CharField(max_length=1000, null=True, blank=True)
	timetocall 	= models.CharField(max_length=1000, null=True, blank=True)
	dispose 		= models.CharField(max_length=1000, null=True, blank=True)
	call_start 	= models.CharField(max_length=100 , null=True, blank=True)
	call_end 		= models.CharField(max_length=100, null=True, blank=False)
	lead_start 	= models.CharField(max_length=100, null=True, blank=True)
	lead_status = models.CharField(max_length=1000, null=True, blank=True)
	agent				= models.ForeignKey(User, null=False, blank=False, on_delete=models.CASCADE)
	lead 				= models.ForeignKey(Lead, null=False, blank=False, on_delete=models.CASCADE)
	updated_at	= models.DateTimeField(null=False, blank=False, auto_now=True)
	created_at	= models.DateTimeField(null=False, blank=False, auto_now_add=True)

	class Meta:
		db_table = "CustomerCall"
		verbose_name = "CustomerCall"
		verbose_name_plural = "CustomerCalls"

	def __str__(self):
		return u'%s %s' % (self.agent, self.dispose)

class UnReachableQue(models.Model):
	participant_id 		= models.CharField(max_length=9999, null=True, blank=True, unique=True)
	count = models.CharField(max_length=9999, null=True, blank=True)
	cell = models.CharField(max_length=9999, null=True, blank=True)
	max_reached = models.BooleanField(default=False)
	lead 				= models.ForeignKey(Lead, null=False, blank=False, on_delete=models.CASCADE)

	updated_at	= models.DateTimeField(null=False, blank=False, auto_now=True)
	created_at	= models.DateTimeField(null=False, blank=False, auto_now_add=True)

	class Meta:
		db_table = "UnReachableQue"
		verbose_name = "UnReachableQue"
		verbose_name_plural = "UnReachableQues"

	def __str__(self):
		return u'%s %s' % (self.participant_id, self.count)

class CallBackQue(models.Model):
	lead 		= models.ForeignKey(Lead, null=False, blank=False, on_delete=models.CASCADE)
	agent_id= models.CharField(max_length=9999, null=True, blank=True)
	cell = models.CharField(max_length=9999, null=True, blank=True)
	datetocall = models.CharField(max_length=1000, null=True, blank=True)
	timetocall = models.CharField(max_length=1000, null=True, blank=True)
	completed = models.BooleanField(default=False)

	updated_at	= models.DateTimeField(null=False, blank=False, auto_now=True)
	created_at	= models.DateTimeField(null=False, blank=False, auto_now_add=True)

	class Meta:
		db_table = "CallBackQue"
		verbose_name = "CallBackQue"
		verbose_name_plural = "CallBackQues"

	def __str__(self):
		return u'%s %s' % (self.cell, self.id)
