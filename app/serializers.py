from rest_framework import serializers
from accounts.serializers import *
from .models import *
from django.contrib.auth import get_user_model
User = get_user_model()


class LeadSerializer(serializers.ModelSerializer):
	class Meta:
		model = Lead
		fields = '__all__'
