# from rest_framework.urlpatterns import format_suffix_patterns
from app import views
from django.views.decorators.cache import cache_page
from django.urls import path

urlpatterns = [
  path('', views.HomePageView.as_view(), name='index'),
  
  path('lead_upload/', views.UploadLead.as_view(), name='lead_upload'),
  path('get_leads/', views.GetLeadsAPIView.as_view(), name='get_leads'),
  path('oncall_status/', views.OnCallStatusAPIView.as_view(), name='join_oncall_status'),
  path('lead_details/<slug:pk>/', views.LeadDetailsAPIView.as_view(), name='lead_details'),
  path('create_call/', views.CreateCallAPIView.as_view(), name='create_call'),
  path('call_backs/', views.CallBacksAPIView.as_view(), name='call_backs'),
  path('unreachables_call/', views.UnreachablesAPIView.as_view(), name='unreachables_call'),
  path('lead_template/', views.LeadTemplateView.as_view(), name='lead_template'),
  path('filter_report/<slug:datefrom>/<slug:dateto>/',views.ReportAPIView.as_view(), name='filter_report'),
  path('download_report/<slug:datefrom>/<slug:dateto>/',views.DownloadReportAPIView.as_view(),name='download_report'),

  path('not_oncall/', views.onCallLeadsAPIView.as_view(), name='not_oncall'),
  path('update_leads/', views.updateLeadsAPIView.as_view(), name='update_leads'),
]
