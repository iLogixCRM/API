from django.http import HttpResponse
import csv

def leadsTemplate():
	try:
		response = HttpResponse(content_type='text/csv')
		response['Content-Disposition'] = 'attachment; filename="iLogix-Leads-Upload-Template.csv"'
		writer = csv.writer(response)
		writer.writerow([
			'cell',
			'name',
			'location',
		])
		return response
	except Exception as e:
		raise e
