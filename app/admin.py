from django.contrib import admin
from .models import *

def undialedLeads(self, request, queryset):
	queryset = queryset.filter(on_call=True)
	for i in queryset:
		try:
			lead = Lead.objects.get(id=i.id)
			lead.on_call = False
			lead.save()
		except Lead.DoesNotExist as e:
			if str(e) == 'Lead matching query does not exist.':		
				pass
undialedLeads.short_description = 'Make not on call'

class OnCallLeads(admin.ModelAdmin):
	list_display = ['id', 'created_at','on_call']
	actions = [undialedLeads, ]  # <-- Add the list action function here

# admin.site.register(Lead, OnCallLeads)
admin.site.register(Lead)
admin.site.register(CustomerCall)
admin.site.register(UnReachableQue)
admin.site.register(CallBackQue)
