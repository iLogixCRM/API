from __future__ import absolute_import
import os
from celery import Celery

# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'proj.settings')

from django.conf import settings

# app = Celery('proj', backend='rpc://', broker='amqp://guest@localhost//')
# app = Celery('proj', broker='amqp://guest@localhost:5672//', backend='rpc://')
app = Celery('proj')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(settings.INSTALLED_APPS)

@app.task(bind=True)
def debug_task(self):
	print('Request: {0!r}'.format(self.request))

if __name__ == '__main__':
  app.start()
